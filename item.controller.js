//CREA UN CONTROLLER PER L'OGGETTO
angular.module("app").controller("ItemCtrl", function($scope) {
  $scope.arma = {
		nome: "Ascia bipenne",
		descrizione: "L’ascia bipenne è una scure a due lame, simbolo del potere minoico.",
		slots: "3",
		image: "http://www.softairgun.eu/images/FOTO/Medioevo/ascia_odino.jpg?osCsid=b8487a7b7bff23dae6b9ec792e1d4185"
	};

	//METODO PER MOSTRARE LA DESCRIZIONE DELL'ARMA
	$scope.mostraDescrizione = function() {
		$scope.descrizione = $scope.arma.descrizione;
		console.log("here");
	};
});
